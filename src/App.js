import React, { Component, useState } from 'react';
import Todos from "./component/Todos";
import Notes from "./component/Notes";
import OneginiContext from "./component/onegini-context";
import XLabel from "./component/XLabel";

const app = () => {

  const [index, setIndex] = useState(0);

  return (
      <div className="App">
        <OneginiContext.Provider value={{x: index, setX: setIndex}}>
          <Notes/>
          <XLabel/>
        </OneginiContext.Provider>
      </div>
  );

};

export default app;
