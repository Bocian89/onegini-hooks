import React, {Component} from 'react';
import {Container, Grid, Label, Input, Button} from 'semantic-ui-react';
import Todo from './Todo';
import axios from 'axios';

class Todos extends Component {

  state = {
    title: '',
    text: '',
    todos: []
  };

  mouseMoveHandler = (event) => {
    console.log(event.clientX, event.clientY);
  };

  componentDidMount() {
    axios.get('http://localhost:8080/todos').then(result => {
      const todosData = result.data;
      let todos = [];
      todosData.forEach(todo => todos.push({
        title: todo.title,
        text: todo.text
      }));
      this.setState({todos: todos});
    });

    window.addEventListener('mousemove', this.mouseMoveHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('mousemove', this.mouseMoveHandler);
  }


  handleChange = (event) => {
    console.log(this.state);
    this.setState({[event.target.name]: event.target.value});
  };

  handleAddTodo = () => {
    this.setState({todos: this.state.todos.concat({
        title: this.state.title,
        text: this.state.text
      })})
  };

  render() {
    return (
      <Container>
        <Grid columns={2}>
          <Grid.Row>
            <Grid.Column>
              {this.state.todos.map((todo, index) => (<Todo title={todo.title} text={todo.text} key={index}/>))}
            </Grid.Column>
            <Grid.Column>
              <Label>
                Title:
                <Input onChange={this.handleChange} name={"title"}/>
              </Label>
              <Label>
                Text:
                <Input onChange={this.handleChange} name={"text"}/>
              </Label>
              <Button onClick={this.handleAddTodo} color="facebook">Add todo</Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    )
  }
}

export default Todos;