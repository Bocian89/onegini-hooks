import React, {useState, useEffect} from 'react';
import Note from './Todo';
import {Segment, Label, Input, Button} from 'semantic-ui-react';
import axios from 'axios';

const notes = (props) => {
  const [title, setTitle] = useState('');
  const [text, setText] = useState('');
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:8080/todos').then(result => {
      console.log(notes);
      setNotes(result.data);
    });
  }, []);

  const mouseMoveHandler = (event) => {
    console.log(event.clientX, event.clientY);
  };

  useEffect(() => {
    window.addEventListener('mousemove', mouseMoveHandler);
    return () =>  {
      window.removeEventListener('mousemove', mouseMoveHandler);
      console.log('Cleaning up');
    };
  }, []);

  // const [noteState, setNote] = useState({
  //   title: '',
  //   text: 'someText'
  // });

  const handleTitleChange = (event) => {
    setTitle(event.target.value);

    // setNote({title: event.target.value});
    // console.log(noteState);
  };

  const handleTextChange = (event) => {
    setText(event.target.value);
    // console.log(noteState);
  };

  const handleAddNote = () => {
    setNotes(notes.concat({
      title, text
    }));
  };

  return (
    <Segment>
      <Label>
        Title:
        <Input onChange={handleTitleChange}/>
      </Label>
      <Label>
        Text:
        <Input onChange={handleTextChange}/>
      </Label>
      <Button color="facebook" onClick={handleAddNote}>
        Add note
      </Button>
      <div>
        {notes.map((todo, index) => (<Note title={todo.title} text={todo.text} key={index}/>))}
      </div>

    </Segment>
  );
};

export default notes;