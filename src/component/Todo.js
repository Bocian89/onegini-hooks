import React from 'react';
import {Message} from 'semantic-ui-react';

const todo = (props) => {
    return (
      <Message>
          <Message.Header>{props.title}</Message.Header>
          <p>{props.text}</p>
      </Message>
    );
};

export default todo;