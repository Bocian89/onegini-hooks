import React, {useContext} from 'react'
import OneginiContext from './onegini-context';
import {Segment} from "semantic-ui-react";

const xlabel = () => {
    const {x} = useContext(OneginiContext);

    return (
        <Segment>{x}</Segment>
    )
};
export default xlabel;