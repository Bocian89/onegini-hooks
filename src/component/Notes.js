import React, {useState, useEffect, useContext} from 'react';
import {Input, Label, Button, Segment} from 'semantic-ui-react';
import Todo from "./Todo";
import axios from 'axios';
import OneginiContext from './onegini-context';

const notes = () => {
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');
    const [notes, setNotes] = useState([]);

    const {x, setX} = useContext(OneginiContext);

    useEffect(() => {
       axios.get('http://localhost:8080/notes').then(result => {
           setNotes(result.data);
           console.log(notes);
       })
    }, []);

    const mouseMoveHandler = (event) => {
        console.log(event.clientX, event.clientY);
    };

    useEffect(() => {
        window.addEventListener('mousemove', mouseMoveHandler);

        return () => window.removeEventListener('mousemove', mouseMoveHandler);
    }, []);

    const titleChangedHandler = (event) => {
      setTitle(event.target.value);
      setX(x + 1);
    };

    const textChangedHandler = (event) => {
        setText(event.target.value);
    };

    const handleAddNoteHandler = () => {
      setNotes(notes.concat({
          title, text
      }))
    };

    return (
        <Segment>
            <Label>
                Title:
                <Input onChange={titleChangedHandler}/>
            </Label>
            <Label>
                Text
                <Input onChange={textChangedHandler}/>
            </Label>
            <Button onClick={handleAddNoteHandler}>Add note</Button>
            <Label>
                {notes.map((note, index) =>
                    <Todo key={index} title={note.title} text={note.text}/>
                )}
            </Label>
        </Segment>
    );
};

export default notes;