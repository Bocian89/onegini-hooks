import React, {Component} from 'react';
import {Segment, Input, Label, Button, Grid} from 'semantic-ui-react';
import Todo from "./Todo";
import axios from 'axios';

class Todos extends Component {

    state = {
      title: '',
      text: '',
      todos: []
    };

    mouseMoveHandler = (event) => {
        console.log(event.clientX, event.clientY);
    };

    componentDidMount() {
        axios.get('http://localhost:8080/todos').then(result => {
            this.setState({todos: result.data});
        });

        window.addEventListener('mousemove', this.mouseMoveHandler)
    }

    componentWillUnmount() {
        window.removeEventListener('mousemove', this.mouseMoveHandler)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    handleChange = (event) => {
        this.setState({[event.target.name] : event.target.value});
    };

    handleAddTodo = () => {
        this.setState({todos: this.state.todos.concat({
                title: this.state.title,
                text: this.state.text
            })});
    };

    render() {
        return (
            <Segment>
               <Grid columns={2}>
                   <Grid.Column>
                       <Grid.Row>
                           <Label>
                               Title:
                               <Input onChange={this.handleChange} name="title"/>
                           </Label>
                       </Grid.Row>

                       <Grid.Row>
                           <Label>
                               Text:
                               <Input onChange={this.handleChange} name="text"/>
                           </Label>
                       </Grid.Row>
                       <Grid.Row>
                           <Button color="facebook" onClick={this.handleAddTodo}>Add todo</Button>
                       </Grid.Row>
                   </Grid.Column>
                   <Grid.Column>
                       {this.state.todos.map((todo, index) =>
                           <Grid.Row key={index}>
                               <Todo title={todo.title} text={todo.text}/>
                           </Grid.Row>
                       )}
                   </Grid.Column>
               </Grid>
            </Segment>
        );
    }
}

export default Todos;